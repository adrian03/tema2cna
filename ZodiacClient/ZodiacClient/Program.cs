﻿using Grpc.Core;
using Grpc.Net.Client;
using System;

namespace ZodiacClient
{
    class Program
    {
        private static GrpcChannel channel;
        private static GetZodiacService.GetZodiacServiceClient client;

        static void Main(string[] args)
        {
            channel = GrpcChannel.ForAddress("https://localhost:5001");
            client = new GetZodiacService.GetZodiacServiceClient(channel);

            char option = ' ';
            do
            {
                try
                {
                    Console.WriteLine("S - Send data to the server");
                    Console.WriteLine("E - Exit the app");
                    option = Console.ReadKey().KeyChar;
                    Console.Clear();
                    switch (option)
                    {
                        case 'S':
                            {
                                Console.WriteLine("\nInsert birthdate:");
                                var date = Console.ReadLine();
                                if (String.IsNullOrWhiteSpace(date))
                                {
                                    Console.WriteLine("\nInvalid birthdate\n");
                                    break;
                                }

                                var result = client.GetZodiacDataRequest(new Date
                                {
                                    CalendaristicDate = date
                                });

                                if (result.ZodiacName.ToString() != String.Empty)
                                {
                                    Console.WriteLine("\nClient has the zodiac " + result.ZodiacName.ToString() + "\n");
                                }
                                break;
                            }

                        case 'E':
                            {
                                Console.WriteLine("\nExit the application\n");
                                break;
                            }
                        default:
                            {
                                Console.WriteLine("\nInvalid option\n");
                                break;
                            }
                    }
                }
                catch (RpcException e)
                {
                    Console.WriteLine(e.Status.Detail);
                }
            } while (option != 'E');

            channel.ShutdownAsync();
        }
    }
}