// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: Protos/autumn.proto
// </auto-generated>
#pragma warning disable 0414, 1591
#region Designer generated code

using grpc = global::Grpc.Core;

namespace ZodiacAutumnService {
  public static partial class GetAutumnZodiacService
  {
    static readonly string __ServiceName = "greet.GetAutumnZodiacService";

    static readonly grpc::Marshaller<global::ZodiacAutumnService.Date> __Marshaller_greet_Date = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::ZodiacAutumnService.Date.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::ZodiacAutumnService.ZodiacDataResponse> __Marshaller_greet_ZodiacDataResponse = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::ZodiacAutumnService.ZodiacDataResponse.Parser.ParseFrom);

    static readonly grpc::Method<global::ZodiacAutumnService.Date, global::ZodiacAutumnService.ZodiacDataResponse> __Method_GetZodiacDataRequest = new grpc::Method<global::ZodiacAutumnService.Date, global::ZodiacAutumnService.ZodiacDataResponse>(
        grpc::MethodType.Unary,
        __ServiceName,
        "GetZodiacDataRequest",
        __Marshaller_greet_Date,
        __Marshaller_greet_ZodiacDataResponse);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::ZodiacAutumnService.AutumnReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of GetAutumnZodiacService</summary>
    [grpc::BindServiceMethod(typeof(GetAutumnZodiacService), "BindService")]
    public abstract partial class GetAutumnZodiacServiceBase
    {
      public virtual global::System.Threading.Tasks.Task<global::ZodiacAutumnService.ZodiacDataResponse> GetZodiacDataRequest(global::ZodiacAutumnService.Date request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Client for GetAutumnZodiacService</summary>
    public partial class GetAutumnZodiacServiceClient : grpc::ClientBase<GetAutumnZodiacServiceClient>
    {
      /// <summary>Creates a new client for GetAutumnZodiacService</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      public GetAutumnZodiacServiceClient(grpc::ChannelBase channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for GetAutumnZodiacService that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      public GetAutumnZodiacServiceClient(grpc::CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      protected GetAutumnZodiacServiceClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      protected GetAutumnZodiacServiceClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      public virtual global::ZodiacAutumnService.ZodiacDataResponse GetZodiacDataRequest(global::ZodiacAutumnService.Date request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return GetZodiacDataRequest(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::ZodiacAutumnService.ZodiacDataResponse GetZodiacDataRequest(global::ZodiacAutumnService.Date request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_GetZodiacDataRequest, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::ZodiacAutumnService.ZodiacDataResponse> GetZodiacDataRequestAsync(global::ZodiacAutumnService.Date request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return GetZodiacDataRequestAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::ZodiacAutumnService.ZodiacDataResponse> GetZodiacDataRequestAsync(global::ZodiacAutumnService.Date request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_GetZodiacDataRequest, null, options, request);
      }
      /// <summary>Creates a new instance of client from given <c>ClientBaseConfiguration</c>.</summary>
      protected override GetAutumnZodiacServiceClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new GetAutumnZodiacServiceClient(configuration);
      }
    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static grpc::ServerServiceDefinition BindService(GetAutumnZodiacServiceBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_GetZodiacDataRequest, serviceImpl.GetZodiacDataRequest).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the  service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static void BindService(grpc::ServiceBinderBase serviceBinder, GetAutumnZodiacServiceBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_GetZodiacDataRequest, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::ZodiacAutumnService.Date, global::ZodiacAutumnService.ZodiacDataResponse>(serviceImpl.GetZodiacDataRequest));
    }

  }
}
#endregion
