﻿using Grpc.Core;
using Grpc.Net.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ZodiacService.Services
{
    public class MainService : GetZodiacService.GetZodiacServiceBase
    {
        private int day = 0;
        private int month = 0;
        private int year = 0;
        private ServerCallContext context;

        private readonly ILogger<MainService> _logger;
        public MainService(ILogger<MainService> logger)
        {
            _logger = logger;
        }

        private bool CheckDate(string input)
        {
            string[] formats = { "M/d/yyyy", "M/dd/yyyy", "MM/d/yyyy", "MM/dd/yyyy" };
            DateTime dateValue;

            if (DateTime.TryParseExact(input, formats, new CultureInfo("en-US"), DateTimeStyles.None, out dateValue))//verifica formatul
                return true;
            else
                return false;
        }

        private void GetData(string input)
        {
            String[] outputStrings = input.Split("/", 3, StringSplitOptions.RemoveEmptyEntries);
            month = int.Parse(outputStrings[0]);
            day = int.Parse(outputStrings[1]);
            year = int.Parse(outputStrings[2]);
        }

        private ZodiacName GetZodiacName()
        {
            switch (month)
            {
                case 3:
                case 4:
                case 5:
                    
                    return ZodiacName.Capricorn;
                case 6:
                case 7:
                case 8:
                    return ZodiacName.Invalid;
                   
                case 9:
                case 10:
                case 11:
                   
                    return ZodiacName.Pesti;
                case 12:
                case 1:
                case 2:
                   
                    return ZodiacName.Varsator;
                case 0:
                default:
                    return ZodiacName.Invalid;

            }
        }

        public override Task<ZodiacDataResponse> GetZodiacDataRequest(Date request, ServerCallContext context)
        {
            this.context = context;
            var replaceDash = new Regex(@"-");
            request.CalendaristicDate = replaceDash.Replace(request.CalendaristicDate, "/");

            if (CheckDate(request.CalendaristicDate.ToString()))
                GetData(request.CalendaristicDate.ToString());
            else throw new RpcException(new Status(StatusCode.Internal, "The string doesn't represent a valid date"));//schimba textul

            return Task.FromResult(new ZodiacDataResponse
            {
                ZodiacName = GetZodiacName()
            });
        }
    }

}
